/* File: isles.c */

#include <stdio.h>
#include <stdlib.h>
#include "../core.h"
#include "isles.h"

ImgArray classify(MyArray iarr, int nhood) {
    int **arr = malloc(iarr.height*sizeof(void *));
    ImgArray output = {iarr.width, iarr.height, arr};
    for (int y = 0; y < iarr.height; y++) {
        arr[y] = malloc(iarr.width * sizeof(int));
        for (int x = 0; x < iarr.width; x++)
            arr[y][x] = iarr.arr[y][x] > 0 ? 0xffffff : 0;
    }
    core_classify(&output, nhood);
    return output;
}
