/* File: isles.i */
%module isles

%{
#define SWIG_FILE_WITH_INIT
#include "../core.h"
#include "isles.h"
%}

%typemap(in) MyArray iarr (MyArray temp) {
  temp.height = PyList_Size((PyObject *) $input);
  temp.width = PyList_Size(PyList_GetItem((PyObject *) $input, 0));
  temp.arr = malloc(temp.height*sizeof(void *));
  for (int i = 0; i < temp.height; i++) {
    PyObject *l = PySequence_GetItem($input, i);
    temp.arr[i] = malloc(temp.width*sizeof(float));
    for (int j = 0; j < temp.width; j++) {
      PyObject *o = PySequence_GetItem(l,j);
      if (PyNumber_Check(o)) {
        temp.arr[i][j] = (float) PyFloat_AsDouble(o);
      } else {
        PyErr_SetString(PyExc_ValueError,
            "Sequence elements must be numbers");
        return NULL;
      }
    }
  }
  $1 = temp;
}

%typemap(freearg) MyArray iarr {
  if ($1.arr && $1.height) {
    for (int y = 0; y < $1.height; y++)
      free($1.arr[y]);
    free($1.arr);
  }
}

%typemap(out) ImgArray classify {
  int w= $1.w;
  int h= $1.h;
  $result = PyList_New(h);
  for (int y = 0; y < h; y++) {
    PyObject *l = PyList_New(w);
    for (int x = 0; x < w; x++) {
      PyObject *o = PyFloat_FromDouble((double) $1.arr[y][x]);
      PyList_SetItem(l,x,o);
    }
    PyList_SetItem($result,y,l);
    free($1.arr[y]);
  }
  free($1.arr);
}

ImgArray classify(MyArray iarr, int nhood);
