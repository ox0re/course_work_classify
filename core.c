#include <stdio.h>
#include <stdlib.h>
#include "core.h"

#define NOT_OUT_OF_BOUNDS (x || y) \
                    && (c->x+x >= 0) && (c->x+x < im->w) \
                    && (c->y+y >= 0) && (c->y+y < im->h)

Pixel *pop(Stack **s) {
    Pixel *p = (*s)->p;
    Stack *prev = *s;
    if ((*s)->next)
        *s = (*s)->next;
    else
        *s = 0;
    free(prev);
    return p;
}

void push(Stack **s, Pixel *p)
{
    Stack *next = 0;
    int id = 0;
    if (*s) {
        next = *s;
        id = next->id + 1;
    }
    *s = malloc(sizeof(Stack));
    (*s)->next = next;
    (*s)->id = id;
    (*s)->p = p;
}

void pushAll(Stack **s, Stack **s2)
{
    while (*s2)
        push(s, pop(s2));
}

Stack *getCornerNeighbors(ImgArrayPtr im, Pixel *c) {
    Stack *neighbors = 0;
    for (int y = -1; y <= 1; y++)
        for (int x = -1; x <= 1; x++)
            if ((abs(x) == abs(y)) && NOT_OUT_OF_BOUNDS)
                if (im->arr[c->y+y][c->x+x] == 0xffffff) {
                    Pixel *n = malloc(sizeof(Pixel));
                    *n = (Pixel){c->x+x, c->y+y};
                    push(&neighbors, n);
                }
    return neighbors;
}

Stack *getStraightNeighbors(ImgArrayPtr im, Pixel *c) {
    Stack *neighbors = 0;
    for (int y = -1; y <= 1; y++)
        for (int x = -1; x <= 1; x++)
            if ((abs(x) != abs(y)) && NOT_OUT_OF_BOUNDS)
                if (im->arr[c->y+y][c->x+x] == 0xffffff) {
                    Pixel *n = malloc(sizeof(Pixel));
                    *n = (Pixel){c->x+x, c->y+y};
                    push(&neighbors, n);
                }
    return neighbors;
}

void setClass(ImgArrayPtr im, Pixel *p, int c)
{
    im->arr[p->y][p->x] = c;
}

void fillIsland(ImgArrayPtr im, int x, int y, int c, int nhood)
{
    Stack *stack = 0;
    Pixel *current = malloc(sizeof(Pixel));
    *current = (Pixel){x, y};
    push(&stack, current);
    do {
        current = pop(&stack);
        if (nhood & STRAIGHT) {
            Stack *neighbors = getStraightNeighbors(im, current);
            pushAll(&stack, &neighbors);
        }
        if (nhood & CORNER) {
            Stack *neighbors = getCornerNeighbors(im, current);
            pushAll(&stack, &neighbors);
        }
        setClass(im, current, c);
        free(current);
    } while (stack);
}

ImgArrayPtr core_classify(ImgArrayPtr im, int nhood)
{
    int class = 0;
    for (int y = 0; y < im->h; y++)
        for (int x = 0; x < im->w; x++)
            if (im->arr[y][x] == 0xffffff) {
                class++;
                fillIsland(im, x, y, class, nhood);
            }
    printf("class = %d\n", class);
    return im;
}

