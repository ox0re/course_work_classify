# Application #

### Dependencies ###

`gcc`, `gdlib`.

### Compilation ###

```
make
```

### Run ###

```
./main picture.png both
```

You will get `picture-o.png` file which you can deal with further. It contains islands of pixels. Each pixel island has an ID presented by a pixel color value (from `0` to `0xffffff`).

You also can use two other classifying methods `corner` and `straight` or `both` as

```
./main picture.png corner
```

or

```
./main picture.png straight
```

# Python module #

### Dependencies ###

`gcc`, `swig`.

### Compilation ###

```
make
```

### Usage ###

Compilation produces files `_isles.so` and `isles.py` contains the class `isles` and it's only method `classify(img[][], neighborhood)`. Put that files in your working directory to use them.

```
import isles
isles.classyfy(img, neighborhood)
```

The `neighborhood` parameter values are
 
 * 1 for corner
 * 2 for straight
 * 3 for both
