#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <gd.h>

#include "core.h"

char *inputFileName;
char *outputFileName;
int nhood;

void gdFreeImage(gdImagePtr im) {
    if (im->tpixels) {
        for (int i = 0; i < im->sy; i++)
            if (im->tpixels[i])
                gdFree(im->tpixels[i]);
        gdFree(im->tpixels);
    }
    gdFree(im);
}

void freeImgArray(ImgArrayPtr imgArray) {
    if (imgArray->arr && imgArray->h > 0) {
        for (int y = 0; y < imgArray->h; y++)
            if (imgArray->arr[y])
                free(imgArray->arr[y]);
        free(imgArray->arr);
    }
}

int isBinary(gdImagePtr im)
{
    int reallyIs = 1;
    for (int i = 0; i < im->sx; i++)
        for (int j = 0; j < im->sy; j++)
            if (gdImageGetPixel(im, i, j) != 0
                    && (gdImageGetPixel(im, i, j) & 0xff) != 255)
                return 0;
    return reallyIs;
}

gdImagePtr copyImageFrom(gdImagePtr im)
{
    gdImagePtr imr = gdImageCreateTrueColor(im->sx, im->sy);
    for (int i = 0; i < im->sx; i++)
        for (int j = 0; j < im->sy; j++) {
            int p = gdImageGetPixel(im, i, j) ? 0xffffff : 0;
            gdImageSetPixel(imr, i, j, p);
        }
    return imr;
}

gdImagePtr loadImageJPG(char *inputFileName)
{
    FILE *inputFile = fopen(inputFileName, "r");
    gdImagePtr image = gdImageCreateFromJpeg(inputFile);
    fclose(inputFile);
    return image;
}

gdImagePtr loadImagePNG(char *inputFileName)
{
    FILE *inputFile = fopen(inputFileName, "r");
    gdImagePtr image = gdImageCreateFromPng(inputFile);
    fclose(inputFile);
    return image;
}

void saveImagePNG(gdImagePtr image, char *outputFileName)
{
    FILE *outputFile = fopen(outputFileName, "wb");
    gdImagePngEx(image, outputFile, 0);
    fclose(outputFile);
}

char *copyFlenameToPNG(char* inputFileName)
{
    int fileNameLen = strlen(inputFileName);
    outputFileName = malloc(strlen(inputFileName) + 7);
    strcpy(outputFileName, inputFileName);
    int i = 0;
    while (*(outputFileName + fileNameLen - i) != '.' && i < fileNameLen)
        i++;
    if (i > fileNameLen)
        i = 0;
    strcpy(outputFileName + fileNameLen - i, "-o.png\0");
    return outputFileName;
}

void checkArgs(int argc)
{
    if (argc < 3) {
        printf("You must specify who arguments: one PNG input file "
                "and a neighbor checking method.\n");
        exit(-1);
    }
}

void setGlobalVars(int argc, char **argv)
{
    checkArgs(argc);
    inputFileName = argv[1];
    char *method = argv[2];
    if (!strcmp(method, "both")) {
        nhood = BOTH;
    } else if (!strcmp(method, "corner")) {
        nhood = CORNER;
    } else if (!strcmp(method, "straight")){
        nhood = STRAIGHT;
    } else {
        printf("Unknown method %s\n", method);
        exit(-1);
    }
    outputFileName = copyFlenameToPNG(inputFileName);
}

ImgArrayPtr convert_gdImagePtr_To_ImgArray(gdImagePtr im)
{
    ImgArrayPtr imgArray = malloc(sizeof(ImgArray));
    imgArray->h = im->sy;
    imgArray->w = im->sx;
    imgArray->arr = malloc(imgArray->h*sizeof(void *));
    for (int y = 0; y < imgArray->h; y++) {
        imgArray->arr[y] = malloc(imgArray->w*sizeof(int));
        for (int x = 0; x < imgArray->w; x++)
            imgArray->arr[y][x] = gdImageGetPixel(im, x, y);
    }
    return imgArray;
}

gdImagePtr convert_ImgArray_To_gdImagePtr(ImgArrayPtr imgArray)
{
    gdImagePtr imr = gdImageCreateTrueColor(imgArray->w, imgArray->h);
    for (int y = 0; y < imgArray->h; y++)
        for (int x = 0; x < imgArray->w; x++) {
            int p = imgArray->arr[y][x];
            gdImageSetPixel(imr, x, y, p);
        }
    return imr;
}

int main(int argc, char **argv)
{
    setGlobalVars(argc, argv);
    gdImagePtr image1 = loadImagePNG(inputFileName);
    gdImagePtr tmp = image1;
    image1 = copyImageFrom(tmp);
    gdFreeImage(tmp);
    if (!isBinary(image1)) {
        printf("Image must be binarized\n");
        exit(-1);
    }
    ImgArrayPtr imgArray1 = convert_gdImagePtr_To_ImgArray(image1);
    imgArray1 = core_classify(imgArray1, nhood);
    free(image1);
    image1 = convert_ImgArray_To_gdImagePtr(imgArray1);
    freeImgArray(imgArray1);
    saveImagePNG(image1, outputFileName);
    gdFreeImage(image1);
    free(outputFileName);
}
