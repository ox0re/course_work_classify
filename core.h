#define STRAIGHT 2
#define CORNER 1
#define BOTH 3

typedef struct {
    int w;
    int h;
    int **arr;
} ImgArray;

typedef ImgArray *ImgArrayPtr;

typedef struct {
    int x;
    int y;
} Pixel;

typedef struct _Stack {
    Pixel *p;
    int id;
    void *next;
} Stack;

ImgArrayPtr core_classify(ImgArrayPtr im, int nhood);
