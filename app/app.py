#!/bin/python
import numpy as np
from skimage.io import imread, imshow, imsave
from skimage.color import rgb2gray, gray2rgb
import skimage
import cv2
import wx
import isles
import copy

class GUIFrame(wx.Frame):
    def __init__(self, *args, **kw):
        super(GUIFrame, self).__init__(*args, **kw)
        self.SetInitialSize(size=wx.Size(640, 480))
        self.engine = Engine()
        self.stage = 1
        self.MakeMenuBar()
        self.MakeStatusBar()
        self.MakeContent()
        self.image = 0

    def MakeContent(self):
        self.panel = wx.Panel(self)
        self.MakeImageWithBtns()
        alignment = self.MakeAlignment()
        self.panel.SetSizer(alignment)

    def MakeMenuBar(self):
        fileMenu = wx.Menu()
        self.menuFileOpen = fileMenu.Append(wx.ID_OPEN, "&Open Image\tCtrl-O",
                "Open image to perform processing")
        fileMenu.AppendSeparator()
        self.menuFileExit = fileMenu.Append(wx.ID_EXIT)

        helpMenu = wx.Menu()
        aboutItem = helpMenu.Append(wx.ID_ABOUT)

        menuBar = wx.MenuBar()
        menuBar.Append(fileMenu, "&File")
        menuBar.Append(helpMenu, "&Help")

        self.SetMenuBar(menuBar)

        self.Bind(wx.EVT_MENU, self.OnOpen, self.menuFileOpen)
        self.Bind(wx.EVT_MENU, self.OnExit, self.menuFileExit)
        self.Bind(wx.EVT_MENU, self.OnAbout, aboutItem)

    def MakeStatusBar(self):
        self.CreateStatusBar()
        self.SetStatusText("Курсовой проект, Кирюшин, Новиков, гр. 13541/2")

    def MakeImageWithBtns(self):
        emptyImg = wx.Image()
        emptyImg.Create(240,240, clear=True)
        self.engine.source = np.random.random([250, 250])
        self.engine.sourceGray = np.random.random([250, 250])
        self.displayImage()
        self.displaySourceBtnsBox = self.MakeDisplaySourceBtns()
        self.hboxTop = wx.BoxSizer(wx.HORIZONTAL)
        self.hboxTop.Add(self.image, 1,
                flag = wx.EXPAND | wx.ALIGN_LEFT | wx.ALIGN_BOTTOM)
        self.hboxTop.AddStretchSpacer(100)
        self.hboxTop.Add(self.displaySourceBtnsBox, 0,
                flag = wx.ALIGN_RIGHT | wx.ALIGN_TOP)

    def MakeDisplaySourceBtns(self):
        displaySourceBtn = wx.RadioButton(self.panel,
                label="Исходное", style=0)
        displayBinarizedBtn = wx.RadioButton(self.panel,
                label="Бинаризованное", style=0)
        displayClassifiedBtn = wx.RadioButton(self.panel,
                label="Классифицированное", style=0)
        displayClassifyNhood = wx.RadioBox(self.panel,
                choices = ["Диагональное", "Прямое", "Оба"],
                label="Соседство", majorDimension=0, style=0)
        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add(displaySourceBtn, 0)
        vbox.Add(displayBinarizedBtn, 0)
        vbox.Add(displayClassifiedBtn, 0)
        vbox.Add(displayClassifyNhood, 0)
        displaySourceBtn.Bind(wx.EVT_RADIOBUTTON,
                self.OnDisplaySource)
        displayBinarizedBtn.Bind(wx.EVT_RADIOBUTTON,
                self.OnDisplayBinarized)
        displayClassifiedBtn.Bind(wx.EVT_RADIOBUTTON,
                self.OnDisplayClassified)
        displayClassifyNhood.Bind(wx.EVT_RADIOBOX,
                self.OnChangeNhood)
        return vbox

    def MakeSlider(self):
        sliderLabel = wx.StaticText(
                self.panel, label = 'Уровень бинаризации:',
                style = wx.ST_NO_AUTORESIZE)
        self.slider = wx.Slider(self.panel, value = 10,
                minValue = 0, maxValue = 255,
                style = wx.SL_HORIZONTAL | wx.SL_LABELS)
        self.slider.Bind(wx.EVT_SLIDER, self.OnSliderScroll)
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(sliderLabel, 0, flag = wx.ALIGN_LEFT | wx.ALIGN_CENTER)
        hbox.Add(self.slider, 1, flag = wx.ALIGN_LEFT | wx.ALIGN_BOTTOM)
        return hbox

    def MakeAlignment(self):
        vbox = wx.BoxSizer(wx.VERTICAL)
        hboxBottom = self.MakeSlider()
        vbox.Add(self.hboxTop, 0,
                flag = wx.ALIGN_LEFT | wx.ALIGN_TOP)
        vbox.AddStretchSpacer()
        vbox.Add(hboxBottom, 0,
                flag = wx.EXPAND | wx.ALIGN_LEFT | wx.ALIGN_BOTTOM)
        return vbox

    def OnSliderScroll(self, e):
        self.engine.threshold = e.GetEventObject().GetValue()
        self.displayImage()

    def OnDisplaySource(self, e):
        self.stage = 1
        self.displayImage()

    def OnDisplayBinarized(self, e):
        self.stage = 2
        self.displayImage()

    def OnDisplayClassified(self, e):
        self.stage = 3
        self.displayImage()

    def OnChangeNhood(self, e):
        if (e.GetEventObject().GetSelection() == 0):
            self.engine.nhood = 1
        if (e.GetEventObject().GetSelection() == 1):
            self.engine.nhood = 2
        if (e.GetEventObject().GetSelection() == 2):
            self.engine.nhood = 3
        self.displayImage()

    def displayImage(self):
        self.engine.Binarize()
        self.engine.Classify()
        if (self.stage == 1):
            h, w = self.engine.source.shape[:2]
            self.wxbmp = wx.Bitmap.FromBuffer(w, h, self.engine.source)
        elif (self.stage == 2):
            h, w = self.engine.binarized.shape[:2]
            backtorgb = cv2.cvtColor(self.engine.binarized, cv2.COLOR_GRAY2RGB)
            self.wxbmp = wx.Bitmap.FromBuffer(w, h, backtorgb)
        elif (self.stage == 3):
            h, w = self.engine.classified.shape[:2]
            backtorgb = self.engine.classified
            self.wxbmp = wx.Bitmap.FromBuffer(w, h, backtorgb)
        self.image = wx.StaticBitmap(self.panel, bitmap=self.wxbmp)
        self.panel.Refresh()

    def OnExit(self, event):
        self.Close(True)

    def OnOpen(self, event):
        wildcard = "JPEG files (*.jpg)|*.jpg"
        dialog = wx.FileDialog(None, "Choose a file",
                               wildcard=wildcard,
                               style=wx.FD_OPEN)
        if dialog.ShowModal() == wx.ID_OK:
            self.imagePath = dialog.GetPath()
        dialog.Destroy()
        self.engine.source = imread(self.imagePath)
        self.engine.sourceGray = imread(self.imagePath, as_grey=True)
        self.displayImage()

    def OnAbout(self, event):
        wx.MessageBox("Программа Васи и Вовы.",
                      "",
                      wx.OK|wx.ICON_INFORMATION)

class Engine():
    def __init__(self):
        self.threshold = 10;
        self.nhood = 1;

    def Binarize(self):
        while len(self.sourceGray.shape) == 3:
            img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        self.binarized = (self.sourceGray*255 > self.threshold).astype(np.uint8)*255

    def Classify(self):
        mylist = self.binarized.tolist()
        classified = isles.classify(mylist, self.nhood)
        self.classified = np.array(classified).astype(np.uint8)
        self.classified = self.painting(self.classified)

    def painting(self, img):
        a = len(set(img.ravel())) - 1
        withzero = np.unique(img)
        nonzero = np.zeros(len(withzero) - 1)
        nonzero[:] = withzero[1:]
        ''''''
        nob = np.zeros((a, 3))
        img1 = np.zeros((img.shape[0],img.shape[1],3), dtype=np.uint8)
        for i in range(a):
            for j in range(3):
                nob[i,j] = np.random.randint(0,255)
        for i in range(img.shape[0]):
            for j in range(img.shape[1]):
                if img[i,j]!=0:
                    for k in range(a):
                        if img[i,j]==nonzero[k]:
                            for p in range(3):
                                img1[i,j,p] = nob[k,p]
        return img1

if __name__ == '__main__':
    app = wx.App()
    frm = GUIFrame(None, title='Image pixel islands classifier')
    frm.Show()
    app.MainLoop()
