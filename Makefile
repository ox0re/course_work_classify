CC=gcc
FILES:=$(wildcard *.c)
FILES:=$(FILES:.c=.o)

CFLAGS += -Wall
CFLAGS += -std=c99
CFLAGS += -O3
CFLAGS += -g
CFLAGS += -lm
CFLAGS += -lgd
CFLAGS += -lz
CFLAGS += -lpng
CFLAGS += -ljpeg

all: main

main: $(FILES) 
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -f main $(FILES)
